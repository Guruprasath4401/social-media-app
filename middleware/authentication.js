const jwt = require('jsonwebtoken');
const User = require('../model/User');

module.exports = () => {
  return (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) {
      return res.status(401).json('You must be log into your account');
    }
    jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
      if (err) {
        return res.status(401).json('Error: Access Denied');
      }
      const { _id } = decoded;
      User.findById(_id).then((userData) => {
        req.user = userData;
        next();
      });
    });
  };
};
