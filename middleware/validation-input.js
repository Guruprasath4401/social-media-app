const validator = require('../validate');

const login = (req, res, next) => {
  const validationRule = {
    email: 'required|email',
    password: 'required|string|min:6',
  };
  validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(412)
        .send({
          success: false,
          message: 'Validation failed for login',
          data: err,
        });
    } else {
      next();
    }
  });
};

const signup = (req, res, next) => {
  const validationRule = {
    username: 'required|string',
    email: 'required|email',
    password: 'required|string|min:6',
  };
  validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(412)
        .send({
          success: false,
          message: 'Validation failed for signup',
          data: err,
        });
    } else {
      next();
    }
  });
};
const updateUser = (req, res, next) => {
  const validationRule = {
    about: 'string',
    userId: 'required|string',
    city: 'string',
    relationship: 'string',
    from: 'string',
  };
  validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(412)
        .send({
          success: false,
          message: 'Validation failed for user update',
          data: err,
        });
    } else {
      next();
    }
  });
};

const createPost = (req, res, next) => {
  const validationRule = {
    userId: 'required|string',
    description: 'required|string',
  };
  validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(412)
        .send({
          success: false,
          message: 'Validation failed for create post',
          data: err,
        });
    } else {
      next();
    }
  });
};

const updatePost = (req, res, next) => {
  const validationRule = {
    userId: 'required|string',
    description: 'required|string',
  };
  validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(412)
        .send({
          success: false,
          message: 'Validation failed for update post',
          data: err,
        });
    } else {
      next();
    }
  });
};

module.exports = {
  signup,
  login,
  updateUser,
  createPost,
  updatePost,
};
