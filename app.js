const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();
const multer = require('multer');
const cloudinary = require('cloudinary').v2;
const { CloudinaryStorage } = require('multer-storage-cloudinary');

const uri = process.env.ATLAS_URI;

const app = express();

const user = require('./routes/user');
const auth = require('./routes/auth');
const post = require('./routes/post');

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/user', user);
app.use('/auth', auth);
app.use('/post', post);

const imageStorage = new CloudinaryStorage({
  cloudinary,
  folder: 'images',
  allowedFormats: ['png', 'jpg', 'jpeg'],
  transformation: [{
    width: 600,
    height: 600,
    crop: 'limit',
  }],
});

mongoose.Promise = global.Promise;

mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
const { connection } = mongoose;
connection.once('open', () => {
  console.log('Connected Database Successfully');
});

app.use(multer({ storage: imageStorage }).single('image'));

app.listen(process.env.PORT || 8080, () => {
  console.log('Server is listening');
});
