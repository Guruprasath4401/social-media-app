const User = require('../model/User');

const Update = async (data, userData) => {
  if (data.userId === userData.id || data.isAdmin) {
    try {
      const user = await User.findByIdAndUpdate(userData.id, {
        $set: data,
      });
      const message = 'Account has been updated';
      const status = 200;
      return { message, status };
    } catch (err) {
      const status = 500;
      return { status };
    }
  } else {
    const message = 'You can update only your account!';
    const status = 403;
    return { message, status };
  }
};

const Delete = async (data, userData) => {
  if (data.userId === userData.id || data.isAdmin) {
    try {
      await User.findByIdAndDelete(userData.id);
      const message = 'Account has been deleted';
      const status = 200;
      return { message, status };
    } catch (err) {
      const status = 503;
      return { status };
    }
  } else {
    const message = 'You can delete only your account!';
    const status = 403;
    return { message, status };
  }
};

const Display = async (userData) => {
  try {
    const user = await User.findById(userData.id);
    const { password, updatedAt, ...other } = user._doc;
    const status = 200;
    return { status, other };
  } catch (err) {
    const status = 500;
    return { status };
  }
};

const Getfriends = async (userData) => {
  try {
    const user = await User.findById(userData.id);
    const friends = await Promise.all(
      user.followings.map((friendId) => {
        return User.findById(friendId);
      }),
    );
    const friendList = [];
    friends.map((friend) => {
      const { _id, username, profilePicture } = friend;
      friendList.push({ _id, username, profilePicture });
    });
    const status = 200;
    return { status, friendList };
  } catch (err) {
    const status = 500;
    return { status };
  }
};

const Follow = async (data, userData) => {
  if (data.userId !== userData.id) {
    try {
      const user = await User.findById(userData.id);
      const currentUser = await User.findById(data.userId);
      if (!user.followers.includes(data.userId)) {
        await user.updateOne({ $push: { followers: data.userId } });
        await currentUser.updateOne({ $push: { followings: userData.id } });
        await user.updateOne({ $push: { request: data.userId } });
        if (currentUser.request.includes(userData.id)) {
          await currentUser.updateOne({ $pull: { request: userData.id } });
          await user.updateOne({ $pull: { request: data.userId } });
        }
        const message = 'user has been followed';
        const status = 200;
        return { message, status };
      }
      const message = 'you allready follow this user';
      const status = 403;
      return { message, status };
    } catch (err) {
      const status = 500;
      return { status };
    }
  } else {
    const message = 'you cant follow yourself';
    const status = 403;
    return { message, status };
  }
};
const Unfollow = async (data, userData) => {
  if (data.userId !== userData.id) {
    try {
      const user = await User.findById(userData.id);
      const currentUser = await User.findById(data.userId);
      if (user.followers.includes(data.userId)) {
        await user.updateOne({ $pull: { followers: data.userId } });
        await currentUser.updateOne({ $pull: { followings: userData.id } });
        await currentUser.updateOne({ $pull: { request: userData.id } });
        const message = 'user has been unfollowed';
        const status = 200;
        return { message, status };
      }
      const message = 'you dont follow this user';
      const status = 403;
      return { message, status };
    } catch (err) {
      const status = 500;
      return { status };
    }
  } else {
    const message = 'you cant unfollow yourself';
    const status = 403;
    return { message, status };
  }
};
const Decline = async (data, userData) => {
  if (data.userId !== userData.id) {
    try {
      const user = await User.findById(userData.id);
      const currentUser = await User.findById(data.userId);
      if (user.followings.includes(data.userId)) {
        await currentUser.updateOne({ $pull: { request: userData.id } });
        const message = 'request has been declined';
        const status = 200;
        return { message, status };
      }
      const message = 'you cannot decline this user';
      const status = 403;
      return { message, status };
    } catch (err) {
      const status = 500;
      return { status };
    }
  } else {
    const message = 'you cant decline yourself';
    const status = 500;
    return { message, status };
  }
};
module.exports = {
  Update,
  Delete,
  Display,
  Getfriends,
  Follow,
  Unfollow,
  Decline,
};
