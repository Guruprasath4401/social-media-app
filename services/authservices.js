const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../model/User');

const Login = async (data) => {
  try {
    const {
      email, password,
    } = data;

    const user = await User.findOne({ email });
    if (!user) {
      const message = 'user not found';
      const status = 404;
      return { message, status };
    }
    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) {
      const message = 'wrong password';
      const status = 400;
      return { message, status };
    }

    const payload = await User.findOne({ email });
    const id = payload._id;
    const token = jwt.sign({ id }, process.env.JWT_KEY);
    const message = 'login Successful';
    const status = 200;
    return { message, token, status };
  } catch (err) {
    const message = 'login failed';
    const status = 500;
    return { message, status };
  }
};

const Signup = async (reguser) => {
  try {
    const {
      username, email, password,
    } = reguser;
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const newUser = new User({
      username,
      email,
      password: hashedPassword,
    });
    const duplicate = await User.findOne({
      email,
      username,
    });
    if (duplicate) {
      const message = 'you have already Registered';
      const status = 400;
      return { message, status };
    }

    const user = await newUser.save();
    const message = 'signup Successfull';
    const status = 200;
    return { message, user, status };
  } catch (err) {
    const message = 'signup failed';
    const status = 500;
    return { message, status };
  }
};

module.exports = {
  Login,
  Signup,
};
