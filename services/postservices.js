const Post = require('../model/Post');
const User = require('../model/User');
const Comments = require('../model/Comments');

const Create = async (data, image) => {
  try {
    const newPost = new Post({
      userId: data.userId,
      description: data.description,
      img: image,
    });
    const savedPost = await newPost.save();
    const status = 200;
    return { savedPost, status };
  } catch (err) {
    const message = 'creating post is failed';
    const status = 500;
    return { message, status };
  }
};

const Update = async (data, postData, image) => {
  try {
    const newPost = new Post({
      userId: data.userId,
      description: data.description,
      img: image,
    });
    const post = await Post.findById(postData.id);
    if (post.userId === data.userId) {
      await post.updateOne({ $set: newPost });
      const message = 'the post has been updated';
      const status = 200;
      return { message, status };
    }
    const message = 'you can update only your post';
    const status = 403;
    return { message, status };
  } catch (err) {
    const message = 'the post has not been updated';
    const status = 500;
    return { message, status };
  }
};

const Delete = async (data, postdata) => {
  try {
    const {
      id,
    } = postdata;
    const post = await Post.findById({ _id: id });
    if (post.userId === data.userId) {
      await post.deleteOne();
      const message = 'the post has been deleted';
      const status = 200;
      return { message, status };
    }
    const message = 'you can delete only your post';
    const status = 403;
    return { message, status };
  } catch (err) {
    const message = 'the post has been already deleted';
    const status = 500;
    return { message, status };
  }
};

const Like = async (data, postData) => {
  try {
    const {
      id,
    } = postData;
    const post = await Post.findById({ _id: id });
    if (!post.likes.includes(data.userId)) {
      await post.updateOne({ $push: { likes: data.userId } });
      const message = 'The post has been liked';
      const status = 200;
      return { message, status };
    }
    await post.updateOne({ $pull: { likes: data.userId } });
    const message = 'The post has been disliked';
    const status = 200;
    return { message, status };
  } catch (err) {
    const status = 500;
    return { status };
  }
};

const AddComment = async (data, postData) => {
  try {
    const {
      id,
    } = postData;
    const {
      text, userId,
    } = data;

    const comments = new Comments({
      text,
      userId,
    });

    comments.save();
    const commentsData = await Post.findByIdAndUpdate(id, { $push: { comment: comments } });
    if (commentsData) {
      const message = 'The post has been commented';
      const status = 200;
      return { message, status };
    }
  } catch (err) {
    const status = 500;
    return { status };
  }
};

const DeleteComment = async (data, postdata) => {
  try {
    const {
      id,
    } = postdata;
    const deleteComment = await Comments.findByIdAndRemove(id);
    const post = await Post.findById(data.postId);
    const updatedPost = await post.updateOne({ $pull: { comment: id } });
    if (deleteComment && updatedPost) {
      const message = 'the comment has been deleted';
      const status = 200;
      return { message, status };
    }
    const message = 'the comment has been already deleted';
    const status = 403;
    return { message, status };
  } catch (err) {
    const message = 'you can delete only your comment';
    const status = 500;
    return { message, status };
  }
};

const GetPost = async (postData) => {
  try {
    const post = await Post.findById(postData.id);
    const status = 200;
    return { status, post };
  } catch (err) {
    const message = 'The post is not avalible';
    const status = 500;
    return { message, status };
  }
};

const Timeline = async (data) => {
  try {
    const currentUser = await User.findById(data.userId);
    const userPosts = await Post.find({ userId: currentUser._id });
    const friendPosts = await Promise.all(
      currentUser.followings.map((friendId) => {
        return Post.find({ userId: friendId });
      }),
    );
    const posts = userPosts.concat(...friendPosts);
    const message = 'The post is not avalible';
    const status = 403;
    return { message, status, posts };
  } catch (err) {
    const message = 'The posts are not avalible';
    const status = 500;
    return { message, status };
  }
};

const All = async (postData) => {
  try {
    const user = await User.findOne({ username: postData.username });
    const posts = await Post.find({ userId: user._id });
    if (posts) {
      const status = 200;
      return { status, posts };
    }
    const message = 'username not found';
    const status = 403;
    return { message, status };
  } catch (err) {
    const status = 500;
    return { status };
  }
};
module.exports = {
  Create,
  Update,
  Delete,
  Like,
  AddComment,
  DeleteComment,
  GetPost,
  Timeline,
  All,
};
