module.exports = {
  linters: {
    '**/*.+(js)': [
      'eslint --fix',
      'prettier --write',
      'jest --findRelatedTests',
      'git add',
    ],
  },
};
