**Social Media App**

Build RESTful APIs for a social media application.

**Objective**

Building a RESTful APIs for a social media application that can be used to create post and view posts of their friends.

**Features**

* User can sign up / log in.
* Users can fill up profile details, upload profile pics.
* Users will have a feed where users can see posts of friends and post their own posts.
* List all the friends.
* Search and send friend requests.
* Accept/decline pending friend requests.

**Technology stack**

     > Node,MongoDB,Express,Jest


**API ENDPOINTS**

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/97d49331d48ff345369a?action=collection%2Fimport)


