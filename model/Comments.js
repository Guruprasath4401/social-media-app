const mongoose = require('mongoose');

const { Schema } = mongoose;

const commentSchema = new Schema({
  text: {
    type: String,
    default: '',
  },
  userId: {
    type: String,
    required: true,
  },
  postedAt: {
    type: Date,
    default: Date.now,
  },
});

const Comment = mongoose.model('Comment', commentSchema);
module.exports = Comment;
