module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    jest: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    'linebreak-style': 0,
    'no-console': 0,
    'no-underscore-dangle': 0,
    'arrow-body-style': 0,
    'array-callback-return': 0,
    'no-unused-vars': 0,
    'consistent-return': 0,
  },
};
