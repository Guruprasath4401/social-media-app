const express = require('express');

const router = express.Router();

const auth = require('../middleware/authentication');
const validationMiddleware = require('../middleware/validation-input');
const services = require('../services/userservices');

router.put('/:id', auth(), validationMiddleware.updateUser, async (req, res) => {
  try {
    const response = await services.Update(req.body, req.params);
    res.status(response.status).json({ message: response.message, post: response.posts });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.delete('/:id', auth(), async (req, res) => {
  try {
    const response = await services.Delete(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.get('/:id', auth(), async (req, res) => {
  try {
    const response = await services.Display(req.params);
    res.status(response.status).json({ message: response.message, about: response.other });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.post('/follow/:id', auth(), async (req, res) => {
  try {
    const response = await services.Follow(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.post('/accept/:id', auth(), async (req, res) => {
  try {
    const response = await services.Follow(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.post('/unfollow/:id', auth(), async (req, res) => {
  try {
    const response = await services.Unfollow(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.post('/decline/:id', auth(), async (req, res) => {
  try {
    const response = await services.Decline(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.get('/friend/:id', auth(), async (req, res) => {
  try {
    const response = await services.Getfriends(req.params);
    res.status(response.status).json({ message: response.message, friends: response.friendList });
  } catch (err) {
    res.status(500).json({ error: err });
  }
});

module.exports = router;
