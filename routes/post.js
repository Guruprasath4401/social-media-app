const express = require('express');

const router = express.Router();

const auth = require('../middleware/authentication');
const validationMiddleware = require('../middleware/validation-input');
const services = require('../services/postservices');

router.post('/create', auth(), validationMiddleware.createPost, async (req, res) => {
  try {
    const image = req.file.path;
    const response = await services.Create(req.body, image);
    res.status(response.status).json({ message: response.message, post: response.savedPost });
  } catch (err) {
    res.status(500).json({ error: 'error occured' });
  }
});

router.put('/:id', auth(), validationMiddleware.updatePost, async (req, res) => {
  try {
    const image = req.file.path;
    const response = await services.Update(req.body, req.params, Image);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.delete('/:id', auth(), async (req, res) => {
  try {
    const response = await services.Delete(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.put('/like/:id', auth(), async (req, res) => {
  try {
    const response = await services.Like(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.post('/comment/:id', auth(), async (req, res) => {
  try {
    const response = await services.AddComment(req.body, req.params);
    res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

router.get('/:id', auth(), async (req, res) => {
  try {
    const response = await services.GetPost(req.params);
    res.status(response.status).json({ message: response.message, post: response.post });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.get('/all', auth(), async (req, res) => {
  try {
    const response = await services.Timeline(req.body, req.params);
    res.status(response.status).json({ message: response.message, post: response.posts });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.get('/profile/:username', auth(), async (req, res) => {
  try {
    const response = await services.All(req.params);
    res.status(response.status).json({ message: response.message, post: response.posts });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.delete('/comment/:id', auth(), async (req, res) => {
  try {
    const response = await services.DeleteComment(req.body, req.params);
    res.status(response.status).json({ message: response.message, error: response.err });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

module.exports = router;
