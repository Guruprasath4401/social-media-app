const express = require('express');

const router = express.Router();
const validationMiddleware = require('../middleware/validation-input');
const services = require('../services/authservices');

router.post('/login', validationMiddleware.login, async (req, res) => {
  try {
    const response = await services.Login(req.body);
    res.status(response.status).json({ message: response.message, token: response.token });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});
router.post('/signup', validationMiddleware.signup, async (req, res) => {
  try {
    const response = await services.Signup(req.body);
    res.status(response.status).json({ message: response.message, data: response.user });
  } catch (err) {
    res.status(500).json({ error: 'error occured ' });
  }
});

module.exports = router;
