const assert = require('assert');
require('dotenv').config();

const User = require('../model/User');
const Post = require('../model/Post');

let testId;
let testName;
let postId;
let postUser;
let res;
let done;
const { dbConnect, dbDisconnect } = require('./utils/dbHandler');

beforeAll(async () => dbConnect());

describe('Unit Testing : Post', () => {
  it('test case for create post', async () => {
    const post = new Post({
      userId: '63ca9265-d9ca-41be-acf4-01ba89c73058',
      desc: 'this is my post',
    });

    await post.save();
    try {
      const findPost = await Post.findOne(post);
      postUser = findPost.userId;
      postId = findPost._id;
      if (findPost.userId === post._id) {
        assert.ok(res.statusCode === 200);
        done();
      }
    } catch (err) {
      expect(201);
    }
  });

  it('test case for update post', async () => {
    try {
      const post = await Post.findById(postId);
      if (post.userId === { _id: postUser }) {
        assert.ok(res.statusCode === 200);
        done();
      }
    } catch (err) {
      expect(201);
    }
  });

  it('test case for delete post', async () => {
    try {
      const post = await Post.findById(postId);
      if (post.userId === { _id: postUser }) {
        await post.deleteOne();
        assert.ok(res.statusCode === 200);
        done();
      }
    } catch (err) {
      expect(201);
    }
  });

  it('test case for adding like to a post', async () => {
    try {
      const post = await Post.findById(postId);
      if (!post.likes.includes(postUser)) {
        await post.updateOne({ $push: { likes: postUser } });
      }
      await post.updateOne({ $pull: { likes: postUser } });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for adding/deleting comments to a post', async () => {
    try {
      const post = await Post.findById(postId);
      const comment = {
        text: 'Hi',
        comment: '63ca9265-d9ca-41be-acf4-01ba89c73058',
      };
      if (!post.comments.includes(postUser)) {
        await post.updateOne({ $push: { comments: comment } });
      }
      await post.updateOne({ $pull: { comments: comment } });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for getting a post', async () => {
    try {
      const post = await Post.findById(postId);
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for timeline post', async () => {
    try {
      const currentUser = await User.findById(testId);
      const userPosts = await Post.find({ userId: currentUser._id });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for all post', async () => {
    try {
      const user = await User.findOne({ username: testName });
      const posts = await Post.find({ userId: user._id });

      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });
});
