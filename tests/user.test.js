const assert = require('assert');
require('dotenv').config();

const User = require('../model/User');

let testUser;
let testId;
let testName;
let res;
let done;

const { fakeData } = require('./utils/mockData');

const { dbConnect, dbDisconnect } = require('./utils/dbHandler');

beforeAll(async () => dbConnect());
afterAll(async () => dbDisconnect());

describe('Unit Testing : User', () => {
  it('Test case for Signup', async () => {
    const user = new User(fakeData);

    await user.save();

    const findUser = await User.findOne(user);
    testUser = findUser.email;
    testId = findUser._id;
    testName = findUser.username;
    if (findUser._id === user._id) {
      assert.ok(res.statusCode === 200);
      done();
    } else {
      expect(201);
    }
  });

  it('test case for login', async () => {
    try {
      await User.findOne(testUser);
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for user Updated', async () => {
    try {
      await User.findById({ _id: testUser });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for user Delete', async () => {
    try {
      await User.findByIdAndDelete(testUser);
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for get friends', async () => {
    try {
      await User.findById(testId);
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for follow', async () => {
    try {
      const user = await User.findById(testId);
      const currentUser = await User.findById({ _id: testId });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for unFollow', async () => {
    try {
      const user = await User.findById(testId);
      const currentUser = await User.findById({ _id: testId });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });

  it('test case for request Decline', async () => {
    try {
      const user = await User.findById(testId);
      const currentUser = await User.findById({ _id: testId });
      assert.ok(res.statusCode === 200);
      done();
    } catch (err) {
      expect(201);
    }
  });
});
